FROM golang:1.21-alpine3.18 AS builder

RUN apk add --no-cache git gcc musl-dev
# Set the working directory inside the container
WORKDIR /app

# Copy the local package files to the container's workspace
COPY go.mod .
COPY go.sum .

COPY go-eth.db /app/

RUN go mod download

COPY . .

# RUN GO111MODULE=on CGO_ENABLED=1
RUN go env -w GO111MODULE=on
RUN go env -w CGO_ENABLED=1

RUN go build -o go-eth .


FROM alpine:3.18
WORKDIR /app
COPY --from=builder /app/go-eth .
COPY --from=builder /app/go-eth.db ./go-eth.db
EXPOSE 8080
CMD ["./go-eth"]
