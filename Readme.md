use go-ethereum library for connect etheruem mainnet and save to db sqlite

how to run 
  * docker-compose up

  or

  * go run main.go
  


api list

Monitoring 
curl --location 'http://localhost:8080/monitoring' \
--header 'Content-Type: application/json' \
--data '{
    "addressList" : ["0x28C6c06298d514Db089934071355E5743bf21d60","0x573906A8eB7E4180e65Ec49d4c53629A58B33263","0xA90f285873a0185f743353f6c30266d14eE5855E"]
}'

Get All Transaction
curl --location 'http://localhost:8080/transaction/getAll'

Get By Address
curl --location 'http://localhost:8080/transaction/0x28C6c06298d514Db089934071355E5743bf21d60'