package main

import (
	"go-ethereum/models"
	"go-ethereum/service"
	"log"
	"net/http"

	"github.com/ant0ine/go-json-rest/rest"
)

func main() {
	models.SetUpDatabase()
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	router := route()
	api.SetApp(router)
	log.Fatal(http.ListenAndServe(":8080", api.MakeHandler()))
}

func route() rest.App {
	route, _ := rest.MakeRouter(
		rest.Get("/transaction/getAll", service.GetAllTransaction),
		rest.Get("/transaction/:address", service.GetByAddress),
		rest.Post("/monitoring", service.MonitorTransaction),
	)
	return route
}
