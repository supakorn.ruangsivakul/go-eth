package service

import (
	"context"
	"fmt"
	"go-ethereum/goeth"
	"go-ethereum/models"
	"log"
	"strconv"

	"github.com/ant0ine/go-json-rest/rest"
	"github.com/ethereum/go-ethereum/core/types"
)

type reqAddress struct {
	AddressList []string
}

var req reqAddress

func MonitorTransaction(w rest.ResponseWriter, r *rest.Request) {
	err := r.DecodeJsonPayload(&req)
	if err != nil {
		w.WriteJson(err)
		return
	}
	//get client with websocket url
	client := goeth.GetClientWSS()
	if client == nil {
		log.Fatalf("Failed to connect to the Ethereum network")
		return
	}

	headers := make(chan *types.Header)
	sub, err := client.SubscribeNewHead(context.Background(), headers)
	if err != nil {
		log.Fatalf("Failed to subscribe to new headers: %v", err)
	}

	db := models.GetDB()
	for {
		select {
		case err := <-sub.Err():
			log.Fatalf("Subscription error: %v", err)
		case header := <-headers:
			block, err := client.BlockByHash(context.Background(), header.Hash())
			if err != nil {
				log.Println("Failed to fetch block:", err)
				continue
			}
			transactionQueue := make(chan *models.Transactions, 10000)

			// Start the goroutine for processing transactions
			go func() {
				for txn := range transactionQueue {
					if IsMonitoring(txn.FromAddress) || IsMonitoring(txn.ToAddress) {
						db.Create(&txn)
						fmt.Println(txn)
					}
				}
			}()

			// Process the transactions
			for idx, tx := range block.Transactions() {
				from, err := client.TransactionSender(context.Background(), tx, block.Hash(), uint(idx))
				if err != nil {
					log.Println("Failed to get transaction sender:", err)
					continue
				}
				toAddress := ""
				if tx.To() != nil {
					toAddress = tx.To().Hex()
				}
				// Use a local variable to avoid race conditions
				txn := &models.Transactions{
					TransactionHash: tx.Hash().Hex(),
					FromAddress:     from.Hex(),
					ToAddress:       toAddress,
					Value:           strconv.Itoa(int(tx.Value().Uint64())),
					GasLimit:        strconv.Itoa(int(tx.Gas())),
					GasPrice:        strconv.Itoa(int(tx.GasPrice().Uint64())),
					TxnTime:         tx.Time().Format("2006-01-02 15:04:05"),
					BlockNumber:     block.Number().String(),
					BlockHash:       block.Hash().Hex(),
				}
				transactionQueue <- txn
			}
		}
	}
}

func IsMonitoring(txnAddr string) bool {
	for _, address := range req.AddressList {
		if txnAddr == address {
			return true
		}
	}
	return false
}
