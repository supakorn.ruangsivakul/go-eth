package service

import (
	"go-ethereum/models"

	"github.com/ant0ine/go-json-rest/rest"
)

func GetAllTransaction(w rest.ResponseWriter, r *rest.Request) {
	db := models.GetDB()

	result := []models.Transactions{}
	err := db.Find(&result).Error
	if err != nil {
		w.WriteJson(err)
		return
	}
	// db.Delete(result)
	w.WriteJson(result)
}
