package service

import (
	"go-ethereum/models"

	"github.com/ant0ine/go-json-rest/rest"
)

func GetByAddress(w rest.ResponseWriter, r *rest.Request) {
	db := models.GetDB()
	address := r.PathParam("address")
	result := []models.Transactions{}
	err := db.Where("from_address = ?", address).Or("to_address = ?", address).Find(&result).Error
	if err != nil {
		w.WriteJson(err)
		return
	}
	w.WriteJson(result)
}
