package goeth

import (
	"log"

	"github.com/ethereum/go-ethereum/ethclient"
)

var rpcURL = "wss://mainnet.infura.io/ws/v3/cb43244e3d884749915eb63008b8fd8b"

// rpcURL := "https://goerli.infura.io/v3/cb43244e3d884749915eb63008b8fd8b"
// rpcURL := "https://sepolia.infura.io/v3/cb43244e3d884749915eb63008b8fd8b"

func GetClientWSS() *ethclient.Client {

	client, err := ethclient.Dial(rpcURL)
	if err != nil {
		log.Fatalf("Failed to connect to the Ethereum network: %v", err)
		return nil
	}
	return client
}
