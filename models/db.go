package models

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func conectDB() {
	var err error
	db, err = gorm.Open(sqlite.Open("go-eth.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
}

func SetUpDatabase() {
	conectDB()
	db.AutoMigrate(&Transactions{})
}

func GetDB() *gorm.DB {
	return db
}
