package models

import (
	"gorm.io/gorm"
)

type Transactions struct {
	gorm.Model
	TransactionHash string
	BlockHash       string
	BlockNumber     string
	FromAddress     string `gorm:"index"`
	ToAddress       string `gorm:"index"`
	Value           string
	TxnTime         string
	GasLimit        string
	GasPrice        string
}
